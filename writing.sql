CREATE TABLE "writing" (
  "id" bigserial,
  "published_id" bigint,
  "owner_id" int4 NOT NULL,
  "title" varchar(255) COLLATE "pg_catalog"."default",
  "last_published_title" varchar(255) COLLATE "pg_catalog"."default",
  "subtitle" varchar(255) COLLATE "pg_catalog"."default",
  "published_route" varchar(255) COLLATE "pg_catalog"."default" UNIQUE,
  "snippet" varchar(255) COLLATE "pg_catalog"."default",
  "last_published_date" timestamp,
  "last_modified_date" timestamp,
  "is_editing_locked" boolean NOT NULL DEFAULT FALSE,
  CONSTRAINT "writing_pkey" PRIMARY KEY ("id")
);
ALTER TABLE "writing" OWNER TO "postgres";

CREATE TABLE "writing_author" (
  "writing_id" bigint NOT NULL,
  "author_id" int4 NOT NULL,
  CONSTRAINT "writing_author_pkey" PRIMARY KEY ("author_id", "writing_id")
);
ALTER TABLE "writing_author" OWNER TO "postgres";

CREATE TABLE "writing_body" (
  "id" bigint,
  "body" varchar(65535) COLLATE "pg_catalog"."default" NOT NULL,
  CONSTRAINT "writing_body_pkey" PRIMARY KEY ("id")
);
ALTER TABLE "writing_body" OWNER TO "postgres";

CREATE TABLE "author" (
  "id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "username" varchar(255) COLLATE "pg_catalog"."default" NOT NULL UNIQUE,
  CONSTRAINT "author_pkey" PRIMARY KEY ("id")
);
ALTER TABLE "author" OWNER TO "postgres";

ALTER TABLE "writing_author" ADD CONSTRAINT "fk_writing_author_writing_1" FOREIGN KEY ("writing_id") REFERENCES "writing" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "writing_author" ADD CONSTRAINT "fk_writing_author_author_1" FOREIGN KEY ("author_id") REFERENCES "author" ("id");
ALTER TABLE "writing_body" ADD CONSTRAINT "fk_writing_body_writing_1" FOREIGN KEY ("id") REFERENCES "writing" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "writing" ADD CONSTRAINT "fk_writing_author_1" FOREIGN KEY ("owner_id") REFERENCES "author" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;