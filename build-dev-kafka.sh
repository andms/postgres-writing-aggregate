#!/bin/bash

docker login registry.gitlab.com
docker build -t registry.gitlab.com/andms/postgres-writing-aggregate .
docker push registry.gitlab.com/andms/postgres-writing-aggregate

docker-compose up -d

sleep 5

docker cp writings.csv postgres-writing-aggregate:/tmp/writings.csv
docker cp writing-bodies.csv postgres-writing-aggregate:/tmp/writing-bodies.csv

docker cp writing-authors.csv postgres-writing-aggregate:/tmp/writing-authors.csv

docker cp writing-snippets.csv postgres-writing-aggregate:/tmp/writing-snippets.csv

./create-connectors.sh

#wait for kafka sink to populate profile data
sleep 10

docker exec -it postgres-writing-aggregate psql -U 'postgres' -d 'postgres' -c "\COPY writing(title,last_published_title,subtitle,published_route,owner_id,published_id,last_published_date,last_modified_date) FROM '/tmp/writings.csv' DELIMITER ',' CSV HEADER;"
docker exec -it postgres-writing-aggregate psql -U 'postgres' -d 'postgres' -c "\COPY writing_body(id,body) FROM '/tmp/writing-bodies.csv' DELIMITER ',' CSV HEADER;"

docker exec -it postgres-writing-aggregate psql -U 'postgres' -d 'postgres' -c "CREATE TABLE temp_table (id int, snippet varchar(255) );"
docker exec -it postgres-writing-aggregate psql -U 'postgres' -d 'postgres' -c "\COPY temp_table(id,snippet) FROM '/tmp/writing-snippets.csv' DELIMITER ',' CSV HEADER;"
docker exec -it postgres-writing-aggregate psql -U 'postgres' -d 'postgres' -c "UPDATE writing SET snippet = temp_table.snippet FROM temp_table Where writing.id = temp_table.id;"
docker exec -it postgres-writing-aggregate psql -U 'postgres' -d 'postgres' -c "DROP TABLE temp_table;"

docker exec -it postgres-writing-aggregate psql -U 'postgres' -d 'postgres' -c "\COPY writing_author(writing_id,author_id) FROM '/tmp/writing-authors.csv' DELIMITER ',' CSV HEADER;"